Brainstormsessie nieuwe lay-out
===============================

Algemeen
--------

- We gebruiken [Twig](http://twig.sensiolabs.org/) als template engine.
- We gaan [Skeleton](http://getskeleton.com/) forken om makkelijk een mooi responsive css framework te krijgen.
- We gebruiken [Font-Awesome](http://fortawesome.github.io/Font-Awesome/) voor icons
- We gaan afkijken bij [Polygon](http://www.polygon.com/)

Ideeën
------

- Submenu's samenvoegen tot lange scrollbare pagina's die eventueel uitklappen.
    * Hierin is te navigeren met een sidebar. Voorbeeld [Boostrap documentatie](http://getbootstrap.com/css/)
    * Scrollanimatie op de knoppen (de pagina springt er niet naar toe, maar scrollt ernaar toe.)
- De homepage moet professioneler worden als je niet ingelogd bent:
    * Geen poll
    * Geen verjaardagen
    * Een grote "word lid" knop
- Als je wel ingelogd bent heeft de homepage een nieuwsfeed met
    * Forum updates
    * Fotoboek updates
    * Polls (idee: dilemma dinsdag polls)
    * Verjaardagen
    * Bedrijven pagina's / vacatures
    * Commissievacatures
    * Quicklinks (boekenverkoop, tentamens etc)?
    * (Announcements)
    * …
- Algemeen layout
    * Banners faden om de layout rustiger te maken (zoals de FMF doet, maar dan mooier), in overleg met Merel
    * Grote banners als die van StudyStore niet meer toestaan omde layout rustiger te maken, in overleg met Merel
    * Geen schaduwen, gradients of ronde hoeken
    * Een modernere implementatie vinden voor de Agenda
    * Vervang de smiles door [Twitter emoji](https://github.com/twitter/twemoji) of een Font-Awesome achtige implementatie: [Twemoji-Awesome](http://ellekasai.github.io/twemoji-awesome/)
- Ideeën voor het herinrichten van het menu
    * Homeknop vervangen door een Coverlogo
    * Members uit het menu halen en de opties terug laten komen in het "Je bent ingelogd menu" (uitklapmenu onder je username) en quicklinks op de homepage (als we die krijgen)
    * Foto's en forum uit het menu halen voor niet ingelogd
    * Activiteiten pagina met agenda en foto's (activities in het menu)
    * Companies hernomen naar Carreer
    * Study hernoemen naar Education
    * Admin vervangen door een tandwieltje

Plan van aanpak
---------------

1. HTML/CSS responsive mockup maken (met statische data)
2. WebCie (en eventueel Bestuur en leden) vragen om mening.
3. Huidige lay-out weggooien (in een nieuwe branch uiteraard)
4. Template engine introduceren
5. Templates maken van de mockup
6. Testen
7. while !tevreden: Features toevoegen/weggooien; Testen;
8. Mergen met main
9. Klaar!

Optioneel:  
1a. We-zijn-begonnen-borrel  
3a. Jeej-we-mogen-aan-de-slag-borrel  
6a. Jeej-we-hebben-de-lelijke-oude-frontend-weggegooid-borrel  
6b. testomg.svcover.nl  
9a. OMG-we-hebben-een-nieuwe-website-borrel  
9b. WebCieleden worden ereleden :)  