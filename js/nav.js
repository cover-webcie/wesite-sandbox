/* JQuery based script to keep menu fixed after scrolling */
$(window).load(function() {
    $('.dropdown-toggle').dropdown()
});

$(window).bind('scroll', function() {
    if ($(window).scrollTop() > $("#header").height()) {
        $("#top_nav").addClass("fixed");
        $("#header").css("margin-bottom", $("#top_nav").height());
    } else {
        $("#top_nav").removeClass("fixed");
        $("#header").css("margin-bottom", 0);
    }
});
