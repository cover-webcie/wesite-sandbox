Cover Web Design Language
=========================

Cover web design language is a project to create a design language and CSS framework for Covers' web pages, together with a design for a new main website.

Roadmap
-------

1. Mock-up (static data but responsive layout) for the new website in HTML/CSS. This should at least contain:
    - Homepage
    - Homepage (when logged in)
    - One of the following pages: Association, Companies or Studies.
2. Collect input from (in this order) the WebCie, the board and members.
3. Implement and tweak the new layout, as described more detailed in [ideas.md](web-design-language/src/master/ideas.md) (Dutch).
4. Put the new layout live.
5. Generalise the new layout into a design language and build a framework upon it.
6. Convert al other web pages to using the new design language.

Goals
-----
This section is an abstract version of the ideas in [ideas.md](web-design-language/src/master/ideas.md) (Dutch). Please check this file for implementation specific details.

- **Modern, future-ready layout**  
    The website should comply to all modern design standards and be ready for future changes of the perception of beauty. This means that it should be maintainable and easy to update. In practise this means probably means introducing a template engine and thinking about design decisions. 
- **Responsive design**  
    The website should look good and be user friendly on all devices.
- **Professional look**  
    The website should look more professionally to outsiders. Therefore, it will hide some more things for those who are not logged in, for example polls, birthdays, etc. We however should not hide too much, as we should still look like an active association. The homepage should as well contain a huge "Become a member"-button.
- **Well-organised structure**  
    The website should be well-organised to make its content easily accessible to both members and outsiders. All information should be placed on the place where you expect it the most. 
- **Engaging user experience**  
    The website should encourage members to use both the serious and social features of the website. These features include but are not limited to booksale, documents and templates, exam base, dreamspark, wiki, mailing list subscriptions, sticker map and merchandise.
    Members should be able to get up-to date on activities, announcements, companies and social features (forum, photo's, weblog, birthdays) without having to navigate to many different pages.